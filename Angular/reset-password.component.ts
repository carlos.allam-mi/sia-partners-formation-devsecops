import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  newPassword:any;         //Input binding for password field
  confirmNewPassword:any;  //Input binding for confirm password field
                 //Variable to store token
  redirectUrl:any;         //Variable for the URL to be redirected to after password reset is complete

  constructor( private Route: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {

    //Grab query parameters from the URL
    this.route.queryParams.subscribe(params=>{
            this.redirectUrl=params.next;
    })
  }

  //This function is called when submit button is clicked
  handleSubmit(){
    console.log(this.newPassword,this.confirmNewPassword)

    //make an API call reset password in the db

    this.handleRedirectAfterSubmit();

  }

  //This function handles the redirect after password is reset

//This function handles the redirect after password is reset
handleRedirectAfterSubmit(){
this.Route.navigateByUrl(this.redirectUrl)
}}
